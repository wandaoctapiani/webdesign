(function($) {
    "use strict";

    //navbar scripts
    //jquery to Collapse navbar

    $(window).on('scroll load', function() {
		if ($(".navbar").offset().top > 60) {
			$(".fixed-top").addClass("top-nav-collapse");
		} else {
			$(".fixed-top").removeClass("top-nav-collapse");
		}
    });

})(jQuery);